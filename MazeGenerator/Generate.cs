﻿using System;
using System.Text;

namespace MazeGenerator
{
    public class Generate
    {
        static Random random = new Random();
        static int[,] twoStep = new int[,] { { 0, 2 }, { 0, -2 }, { 2, 0 }, { -2, 0 } };
        static int[,] oneStep = new int[,] { { 0, 1 }, { 0, -1 }, { 1, 0 }, { -1, 0 } };

        public static string[] GetMaze(int minSize, int maxSize)
        {
            var heigth = random.Next(minSize, maxSize); ;
            var width = random.Next(minSize, maxSize); ;
            if (heigth % 2 == 0)
                heigth++;
            if (width % 2 == 0)
                width++;
            var maze = new bool[heigth, width];
            GenerateMaze(maze, heigth - 2, width - 2);
            return ConvertMazeToString(maze);
        }

        private static void GenerateMaze(bool[,] maze, int xPosition, int yPosition)
        {
            var direction = GenerateRandomArray();
            for (var index = 0; index < direction.Length; index++)
            {
                if (!CheckRoad(maze, xPosition, yPosition, direction[index])) continue;
                maze[xPosition, yPosition] = true;
                xPosition += oneStep[direction[index], 0];
                yPosition += oneStep[direction[index], 1];
                maze[xPosition, yPosition] = true;
                xPosition += oneStep[direction[index], 0];
                yPosition += oneStep[direction[index], 1];
                maze[xPosition, yPosition] = true;
                GenerateMaze(maze, xPosition, yPosition);
                index = -1;
            }
            return;
        }

        private static bool CheckRoad(bool[,] maze, int xPosition, int yPosition, int direction)
        {
            var x = xPosition + twoStep[direction, 0];
            var y = yPosition + twoStep[direction, 1];
            return !(Math.Min(x, y) < 0 || x >= maze.GetLength(0) || y >= maze.GetLength(1) || maze[x, y]);
        }

        private static int[] GenerateRandomArray()
        {
            var array = new int[4];
            for (var i = 0; i < array.Length; i++)
                array[i] = i;
            for (var i = 0; i < 100; i++)
                Swap(array, random.Next(0, 4), random.Next(0, 4));
            return array;
        }

        private static string[] ConvertMazeToString(bool[,] maze)
        {
            var result = new string[maze.GetLength(0) + 2];
            result[0] = "1 1";
            result[1] = string.Format("{0} {1}", maze.GetLength(1) - 2, maze.GetLength(0) - 2);
            var mazeString = new StringBuilder();
            for (var i = 0; i < maze.GetLength(0); i++)
            {
                for (var j = 0; j < maze.GetLength(1); j++)
                    mazeString.Append(maze[i, j] ? " " : "#");
                result[i + 2] = mazeString.ToString();
                mazeString.Clear();
            }
            return result;
        }

        private static void Swap(int[] array, int a, int b)
        {
            var tmp = array[b];
            array[b] = array[a];
            array[a] = tmp;
        }
    }
}

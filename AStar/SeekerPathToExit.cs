﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace AStar {
    public class SeekerPathToExit {
        public static List<Point> FindPath(Point start, Point finish, bool isDiogonal, Func<Point, bool> IsWall) {
            var pointsInQueue = new List<PointOfPath>();
            var passedPoints = new List<PointOfPath>();
            pointsInQueue.Add(new PointOfPath(start, finish, null));
            PointOfPath lastPoint;
            while (pointsInQueue.Count != 0) {
                lastPoint = pointsInQueue.First(i => i.CostOfWay == pointsInQueue.Min(x => x.CostOfWay));
                for (int y = lastPoint.Point.Y - 1; y <= lastPoint.Point.Y + 1; y++) {
                    for (int x = lastPoint.Point.X - 1; x <= lastPoint.Point.X + 1; x++) {
                        if (!isDiogonal && (x - lastPoint.Point.X) * (y - lastPoint.Point.Y) != 0) continue;
                        var point = new PointOfPath(new Point(x, y), finish, lastPoint);
                        if (IsWall(point.Point)) continue;
                        if (!pointsInQueue.Contains(point)) {
                            if (!passedPoints.Contains(point)) {
                                pointsInQueue.Add(point);
                            }
                            if (point.Point.Equals(finish)) {
                                return CollectResultPath(point);
                            }
                        }
                    }
                }
                passedPoints.Add(lastPoint);
                pointsInQueue.Remove(lastPoint);
            }
            return new List<Point>();
        }

        public static List<Point> CollectResultPath(PointOfPath exitPoint) {
            var resultPath = new List<Point>();
            while (exitPoint.LastPoint != null) {
                resultPath.Add(exitPoint.Point);
                exitPoint = exitPoint.LastPoint;
            }
            resultPath.Add(exitPoint.Point);
            resultPath.Reverse();
            return resultPath;
        }
    }
}

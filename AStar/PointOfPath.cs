﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AStar {
    public class PointOfPath {
        enum CostOfStep {
            Straight = 10,
            Diogonal = 14,
        }

        public Point Point { get; private set; }
        public Point FinishPoint { get; private set; }
        public PointOfPath LastPoint { get; private set; }

        public int PassedWay { get; private set; }
        public int RemainedWay { get; private set; }
        public int CostOfWay { get; private set; }

        public PointOfPath(Point point, Point finishPoint, PointOfPath lastPoint) {
            Point = point;
            FinishPoint = finishPoint;
            LastPoint = lastPoint;
            CalcPassedWay();
            CalcRemainedWay();
            CalcCostOfWay();
        }

        private void CalcPassedWay() {
            if (LastPoint == null) {
                PassedWay = 0;
                return;
            }
            PassedWay = LastPoint.PassedWay;
            if ((Point.X - LastPoint.Point.X) * (Point.Y - LastPoint.Point.Y) == 0)
                PassedWay += (int)CostOfStep.Straight;
            else PassedWay += (int)CostOfStep.Diogonal;
        }

        private void CalcRemainedWay() {
            RemainedWay = (Math.Abs(FinishPoint.X - Point.X) + Math.Abs(FinishPoint.Y - Point.Y)) * (int)CostOfStep.Straight;
        }

        private void CalcCostOfWay() {
            CostOfWay = PassedWay + RemainedWay;
        }

        public override bool Equals(Object obj) {
            if (!(obj is PointOfPath)) return false;
            var point = (PointOfPath)obj;
            return (Point == point.Point);
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }
    }
}

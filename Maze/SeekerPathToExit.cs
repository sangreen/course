﻿
namespace Mazes
{
    class SeekerPathToExit
    {

        public static void MoveOut(Robot robot)
        {
            var resultPath = AStar.SeekerPathToExit.FindPath(robot.maze.Robot, robot.maze.Exit, false, robot.maze.IsWall);

            foreach (var point in resultPath)
            {
                Move(robot, robot.X - point.X, robot.Y - point.Y);
            }
        }

        public static void Move(Robot robot, int dx, int dy)
        {
            if (dx < 0) robot.MoveTo(Direction.Right);
            if (dx > 0) robot.MoveTo(Direction.Left);
            if (dy > 0) robot.MoveTo(Direction.Up);
            if (dy < 0) robot.MoveTo(Direction.Down);
        }

    }

    class FindWay
    {
        public static void MoveOut(Robot robot)
        {
            var resultPoint = WaveAlgorithm.FindWave.FindWay(robot.maze.Robot, robot.maze.Exit, robot.maze.IsWall);

            foreach (var point in resultPoint)
            {
                SeekerPathToExit.Move(robot, robot.X - point.X, robot.Y - point.Y);
            }
        }
    }
}

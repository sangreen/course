﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace WaveAlgorithm
{
    public static class FindWave
    {
        public static List<Point> FindWay(Point start, Point finish, Func<Point, bool> Map)
        {
            bool flag = true;
            var MapWidht = finish.X+1;
            var MapHeight = finish.Y + 1;
            int[,] cMap = new int[MapWidht, MapHeight];
            var step = 0;

            for (var x = 0; x < MapWidht; x++)
                for (var y = 0; y < MapHeight; y++)
                {
                    if (Map(new Point { X = x, Y = y }) == true)
                        cMap[x, y] = -2; //индикатор стены
                    else
                        cMap[x, y] = -1; //индикатор "сюда еще не наступали"
                }
            cMap[finish.X, finish.Y] = 0; //начинаем с финиша

            while (flag == true)
            {
                flag = false;
                for (var x = 0; x < MapWidht; x++)
                    for (var y = 0; y < MapHeight; y++)
                    {
                        if (cMap[x, y] == step) //step=0-финиш
                        {
                            //ставим значение шага+1 в соседние ячейки(если они проходимы)
                            if (y - 1 >= 0 && cMap[x, y - 1] != -2 && cMap[x, y - 1] == -1)
                                cMap[x, y - 1] = step + 1;
                            if (x - 1 >= 0 && cMap[x - 1, y] != -2 && cMap[x - 1, y] == -1)
                                cMap[x - 1, y] = step + 1;
                            if (y + 1 < MapHeight && cMap[x, y + 1] != -2 && cMap[x, y + 1] == -1)
                                cMap[x, y + 1] = step + 1;
                            if (x + 1 < MapWidht && cMap[x + 1, y] != -2 && cMap[x + 1, y] == -1)
                                cMap[x + 1, y] = step + 1;

                        }
                    }
                step++;
                flag = true;
                if (cMap[start.X, start.Y] != -1) //решение найдено
                    flag = true;

                if (step > MapHeight * MapWidht) //решение не найдено
                    flag = false;
            }
            if (cMap[start.X, start.Y] != -1)
                return FindShortestWay(cMap, start.X, start.Y, finish.X, finish.Y);
            return null;
        }
        public static List<Point> FindShortestWay(int[,] Map, int startX, int startY, int finishX, int finishY)
        {
            List<Point> list = new List<Point>();
            var stepAmount = Map[startX, startY];
            var x = startX;
            var y = startY;
            while (stepAmount != 0)
            {
                stepAmount--;
                if (y + 1 < Map.GetLength(1) && Map[x, y + 1] == stepAmount)
                {
                    list.Add(new Point { X = x, Y = y + 1 });
                    y++;
                    continue;
                }
                if (x + 1 < Map.GetLength(0) && Map[x + 1, y] == stepAmount)
                {
                    list.Add(new Point{X=x+1, Y=y });
                    x++;
                    continue;
                }
                if (y - 1 >= 0 && Map[x, y - 1] == stepAmount)
                {
                    list.Add(new Point { X = x, Y = y - 1 });
                    y--;
                    continue;
                }
                if (x - 1 >= 0 && Map[x - 1, y] == stepAmount)
                {
                    list.Add(new Point { X = x - 1, Y = y});
                    x--;
                    continue;
                }

            }
            return list;
        }
    }
}

